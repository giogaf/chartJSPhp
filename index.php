<!DOCTYPE html>
<html>
	<head>
		<title>Chart.js-PHP</title>
		
		<style>
		.chart {
			display: inline-block;border:1px solid black;
		}
		.li_legend  span{
    		display: inline-block;
    		width: 12px;
    		height: 12px;
    		margin-right: 5px;
		}
		ul{
			list-style:none;
		}
		</style>
	</head>
	<body>
		<div class='chart'>
			<h1>Bar</h1>
			<?php
			require_once 'mychart.php';
			echo $Bar;
			?>
		</div>
		<div class='chart'>
			<h1>Pie</h1> 
			<?php
			echo $Pie;
			?>
		</div>
	
		<script src="chartjs/Chart.js"></script>
		<script src="chartjs/chart.js-php.js"></script>
		<script>
	    (function () {
	       	loadChartJsPhp();
	    		})();
		</script>
	</body>
</html>







































