<?php
require 'chartjs/class/ChartJS.php';
require 'chartjs/class/ChartJS_Bar.php';
require 'chartjs/class/ChartJS_Pie.php';



ChartJS::addDefaultColor(array('fill' => '#f2b21a', 'stroke' => '#e5801d', 'point' => '#e5801d', 'pointStroke' => '#e5801d'));
ChartJS::addDefaultColor(array('fill' => 'rgba(28,116,190,.8)', 'stroke' => '#1c74be', 'point' => '#1c74be', 'pointStroke' => '#1c74be'));
ChartJS::addDefaultColor(array('fill' => 'rgba(212,41,31,.7)', 'stroke' => '#d4291f', 'point' => '#d4291f', 'pointStroke' => '#d4291f'));
ChartJS::addDefaultColor(array('fill' => '#dc693c', 'stroke' => '#ff0000', 'point' => '#ff0000', 'pointStroke' => '#ff0000'));
ChartJS::addDefaultColor(array('fill' => 'rgba(46,204,113,.8)', 'stroke' => '#2ecc71', 'point' => '#2ecc71', 'pointStroke' => '#2ecc71'));
ChartJS::addDefaultColor(array('fill' => 'rgba(46,20,11,.6)', 'stroke' => 'rgba(46,20,11,.7)', 'point' => '#2ecc71', 'pointStroke' => '#2ecc71'));
ChartJS::addDefaultColor(array('fill' => 'rgba(28,11,190,.4)', 'stroke' => 'rgba(28,11,190,.6)', 'point' => '#1c74be', 'pointStroke' => '#1c74be'));


$numdata=rand(2,7);
$top=rand(50,200);
$data1=array();
$data2=array();


for ($i = 0; $i < $numdata; $i++) {
	$data1[]=rand(20,$top);
	$data2[]=rand(20,$top);


}

$array_values = array($data1,$data2);
$array_labels = array("January", "February", "March", "April", "May", "June", "July");

$Bar = new ChartJS_Bar('example_line', 200,200);
$Bar->addBars($array_values[0],array('label'=>"Sales Mondongo"));
$Bar->addBars($array_values[1],array('label'=>'Sales Bandeja Paisa'));

$Bar->addLabels($array_labels);


$Pie = new ChartJS_Pie('example_pie', 200, 200);
for ($i = 0; $i < $numdata; $i++) {
	$Pie->addPart($data1[$i]);

}

$Pie->addLabels($array_labels);











